# Compy CMS
### Description
this content managwmwnt system is being built sort of from scratch. various tools available to automate things or get ideas from will be or are used to construct this small cms.

### Documentation
When available most will be at [the main site](https://werxlab.org).

Some documentation may be kept in [the WIKI](../../wikis/home).

### License
[Apache License](LICENSE)

### Demo
When available, You will be able to find it as a subdomain of [WerxLab - chompy.werxlab.org](https://chompy.werxlab.org)
